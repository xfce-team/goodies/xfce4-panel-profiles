# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce-apps.xfce4-panel-profiles package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Marco <marcxosm@gmail.com>, 2023
# Dmytro Tokayev, 2023
# Cristian Marchi <cri.penta@gmail.com>, 2023
# Xfce Bot <transifex@xfce.org>, 2024
# Nick Schermer <nick@xfce.org>, 2024
# Emanuele Petriglia <transifex@emanuelepetriglia.com>, 2024
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: xfce-apps.xfce4-panel-profiles\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2024-11-26 00:49+0100\n"
"PO-Revision-Date: 2018-07-13 15:34+0000\n"
"Last-Translator: Emanuele Petriglia <transifex@emanuelepetriglia.com>, 2024\n"
"Language-Team: Italian (https://app.transifex.com/xfce/teams/16840/it/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: it\n"
"Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: org.xfce.PanelProfiles.desktop.in:3
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:18
msgid "Panel Profiles"
msgstr "Profili del pannello"

#: org.xfce.PanelProfiles.desktop.in:4
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:23
msgid "Backup and restore your panel configuration"
msgstr "Backup e ripristino della configurazione del pannello"

#: org.xfce.PanelProfiles.desktop.in:5
msgid ""
"panel;taskbar;layout;switch;apply;restore;save;backup;remove;import;export;"
msgstr ""
"panel;taskbar;layout;switch;apply;restore;save;backup;remove;import;export;"

#: org.xfce.PanelProfiles.desktop.in:12
msgid "Configuration;User;"
msgstr "Configuration;User;"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:68
#: xfce4-panel-profiles/xfce4-panel-profiles.py:427
msgid "Filename"
msgstr "Nome del file"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:84
msgid "Date Modified"
msgstr "Data di modifica"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:107
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:108
#: xfce4-panel-profiles/xfce4-panel-profiles.py:375
msgid "Apply Configuration"
msgstr "Applica configurazione"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:122
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:123
#: xfce4-panel-profiles/xfce4-panel-profiles.py:332
msgid "Save Configuration"
msgstr "Salva configurazione"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:137
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:138
msgid "Remove Configuration"
msgstr "Rimuovi configurazione"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:152
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:153
msgid "Export"
msgstr "Esporta"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:167
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:168
msgid "Import"
msgstr "Importa"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:195
msgid "_Help"
msgstr "_Aiuto"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:210
msgid "_Close"
msgstr "_Chiudi"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:134
msgid "Current Configuration"
msgstr "Configurazione corrente"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:152
msgid "Today"
msgstr "Oggi"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:154
msgid "Yesterday"
msgstr "Ieri"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:218
#, python-format
msgid "%s (Copy of %s)"
msgstr "%s (Copia di %s)"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:236
msgid "Import configuration file..."
msgstr "Importa file di configurazione…"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:239
#: xfce4-panel-profiles/xfce4-panel-profiles.py:331
#: xfce4-panel-profiles/xfce4-panel-profiles.py:374
#: xfce4-panel-profiles/xfce4-panel-profiles.py:401
#: xfce4-panel-profiles/xfce4-panel-profiles.py:454
#: xfce4-panel-profiles/xfce4-panel-profiles.py:479
msgid "Cancel"
msgstr "Annulla"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:240
msgid "Open"
msgstr "Apri"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:254
msgid ""
"Invalid configuration file!\n"
"Please select a valid configuration file."
msgstr ""
"File di configurazione non valido!\n"
"Selezionare un file di configurazione valido."

#: xfce4-panel-profiles/xfce4-panel-profiles.py:262
msgid "OK"
msgstr "OK"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:325
msgid "Name the new panel configuration"
msgstr "Assegna un nome alla nuova configurazione del pannello"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:349
#, python-format
msgid "Backup_%s"
msgstr "Backup_%s"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:365
msgid ""
"Do you want to apply this configuration?\n"
" The current configuration will be lost!"
msgstr ""
"Applicare questa configurazione?\n"
" La configurazione corrente andrà persa!"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:382
msgid "Make a backup of the current configuration"
msgstr "Eseguire un backup della configurazione corrente"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:393
msgid "Errors occurred while parsing the current configuration."
msgstr ""
"Si sono verificati degli errori durante l'analisi della configurazione "
"attuale."

#: xfce4-panel-profiles/xfce4-panel-profiles.py:402
#: xfce4-panel-profiles/xfce4-panel-profiles.py:458
msgid "Save"
msgstr "Salva"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:413
msgid ""
"Do you want to save despite the errors? Some configuration information could"
" be missing."
msgstr ""
"Salvare nonostante gli errori? Potrebbero mancare alcune informazioni di "
"configurazione."

#: xfce4-panel-profiles/xfce4-panel-profiles.py:421
msgid "Export configuration as..."
msgstr "Esporta configurazione come…"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:433
msgid "Untitled"
msgstr "Senza titolo"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:441
msgid "Location"
msgstr "Posizione"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:445
msgid "Select a Folder"
msgstr "Seleziona una cartella"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:477
#, python-format
msgid "<b>A file named \"%s\" already exists. Do you want to replace it?</b>"
msgstr "<b>Un file col nome \"%s\" è già esistente. Si desidera sostituirlo?</b>"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:478
#, python-format
msgid ""
"The file already exists in \"%s\", replacing it will overwrite its contents."
msgstr ""
"Il file è già esistente come \"%s\", sostituirlo sovrascriverà il suo "
"contenuto."

#: xfce4-panel-profiles/xfce4-panel-profiles.py:480
msgid "Replace"
msgstr "Sostituisci"

#: data/metainfo/org.xfce.PanelProfiles.appdata.xml.in:6
msgid "Xfce Panel Profiles"
msgstr "Xfce Panel Profiles"

#: data/metainfo/org.xfce.PanelProfiles.appdata.xml.in:7
msgid "Application to manage different panel layouts in Xfce"
msgstr ""
"Applicazione per gestire differenti configurazioni del pannello in Xfce"

#: data/metainfo/org.xfce.PanelProfiles.appdata.xml.in:11
msgid ""
"Multitude of panel layouts can be created using Xfce panel. This tool "
"enables managing different layouts with little effort. Xfce4-panel-profiles "
"makes it possible to backup, restore, import and export panel layouts."
msgstr ""
"Con il pannello di Xfce si possono creare diverse configurazioni per i "
"pannelli. Con Xfce4-panel-profiles si possono gestire le diverse "
"configurazioni in modo facile, permettendo di fare backup, ripristinare, "
"importare o esportare configurazioni dei pannelli."
