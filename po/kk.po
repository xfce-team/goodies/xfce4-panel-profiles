# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce-apps.xfce4-panel-profiles package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Xfce Bot <transifex@xfce.org>, 2024
# Baurzhan Muftakhidinov <baurthefirst@gmail.com>, 2024
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: xfce-apps.xfce4-panel-profiles\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2024-11-26 00:49+0100\n"
"PO-Revision-Date: 2018-07-13 15:34+0000\n"
"Last-Translator: Baurzhan Muftakhidinov <baurthefirst@gmail.com>, 2024\n"
"Language-Team: Kazakh (https://app.transifex.com/xfce/teams/16840/kk/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: kk\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#: org.xfce.PanelProfiles.desktop.in:3
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:18
msgid "Panel Profiles"
msgstr "Панель профильдері"

#: org.xfce.PanelProfiles.desktop.in:4
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:23
msgid "Backup and restore your panel configuration"
msgstr "Панель баптауларын қорға көшіру және қалпына келтіру"

#: org.xfce.PanelProfiles.desktop.in:5
msgid ""
"panel;taskbar;layout;switch;apply;restore;save;backup;remove;import;export;"
msgstr ""
"panel;taskbar;layout;switch;apply;restore;save;backup;remove;import;export;панель;тапсырмалар;жайма;ауыстыру;іске"
" асыру;қалпына келтіру;сақтау;қор көшірме;өшіру;импорт;экспорт;"

#: org.xfce.PanelProfiles.desktop.in:12
msgid "Configuration;User;"
msgstr "Баптаулар;Пайдаланушы;"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:68
#: xfce4-panel-profiles/xfce4-panel-profiles.py:427
msgid "Filename"
msgstr "Файл аты"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:84
msgid "Date Modified"
msgstr "Өзгертілген күні"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:107
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:108
#: xfce4-panel-profiles/xfce4-panel-profiles.py:375
msgid "Apply Configuration"
msgstr "Баптауларды іске асыру"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:122
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:123
#: xfce4-panel-profiles/xfce4-panel-profiles.py:332
msgid "Save Configuration"
msgstr "Баптауларды сақтау"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:137
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:138
msgid "Remove Configuration"
msgstr "Баптауларды өшіру"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:152
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:153
msgid "Export"
msgstr "Экспорттау"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:167
#: xfce4-panel-profiles/xfce4-panel-profiles.glade:168
msgid "Import"
msgstr "Импорттау"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:195
msgid "_Help"
msgstr "Кө_мек"

#: xfce4-panel-profiles/xfce4-panel-profiles.glade:210
msgid "_Close"
msgstr "_Жабу"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:134
msgid "Current Configuration"
msgstr "Ағымдағы баптаулар"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:152
msgid "Today"
msgstr "Бүгін"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:154
msgid "Yesterday"
msgstr "Кеше"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:218
#, python-format
msgid "%s (Copy of %s)"
msgstr "%s (%s көшірмесі)"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:236
msgid "Import configuration file..."
msgstr "Баптаулар файлын импорттау..."

#: xfce4-panel-profiles/xfce4-panel-profiles.py:239
#: xfce4-panel-profiles/xfce4-panel-profiles.py:331
#: xfce4-panel-profiles/xfce4-panel-profiles.py:374
#: xfce4-panel-profiles/xfce4-panel-profiles.py:401
#: xfce4-panel-profiles/xfce4-panel-profiles.py:454
#: xfce4-panel-profiles/xfce4-panel-profiles.py:479
msgid "Cancel"
msgstr "Бас тарту"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:240
msgid "Open"
msgstr "Ашу"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:254
msgid ""
"Invalid configuration file!\n"
"Please select a valid configuration file."
msgstr ""
"Конфигурация файлы жарамсыз!\n"
"Жарамды конфигурация файлын таңдаңыз."

#: xfce4-panel-profiles/xfce4-panel-profiles.py:262
msgid "OK"
msgstr "ОК"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:325
msgid "Name the new panel configuration"
msgstr "Жаңа панель баптауларын атау"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:349
#, python-format
msgid "Backup_%s"
msgstr "Қор_көшірме_%s"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:365
msgid ""
"Do you want to apply this configuration?\n"
" The current configuration will be lost!"
msgstr ""
"Бұл конфигурацияны іске асырғыңыз келе ме?\n"
" Ағымдағы конфигурация жоғалатын болады!"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:382
msgid "Make a backup of the current configuration"
msgstr "Ағымдағы конфигурацияның қор көшірмесін жасау"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:393
msgid "Errors occurred while parsing the current configuration."
msgstr ""

#: xfce4-panel-profiles/xfce4-panel-profiles.py:402
#: xfce4-panel-profiles/xfce4-panel-profiles.py:458
msgid "Save"
msgstr "Сақтау"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:413
msgid ""
"Do you want to save despite the errors? Some configuration information could"
" be missing."
msgstr ""
"Қателерге қарамай сақтауды қалайсыз ба? Конфигурацияның кейбір ақпараты жоқ "
"болуы мүмкін."

#: xfce4-panel-profiles/xfce4-panel-profiles.py:421
msgid "Export configuration as..."
msgstr "Баптауларды қалайша экспорттау..."

#: xfce4-panel-profiles/xfce4-panel-profiles.py:433
msgid "Untitled"
msgstr "Атаусыз"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:441
msgid "Location"
msgstr "Орналасуы"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:445
msgid "Select a Folder"
msgstr "Буманы таңдау"

#: xfce4-panel-profiles/xfce4-panel-profiles.py:477
#, python-format
msgid "<b>A file named \"%s\" already exists. Do you want to replace it?</b>"
msgstr ""

#: xfce4-panel-profiles/xfce4-panel-profiles.py:478
#, python-format
msgid ""
"The file already exists in \"%s\", replacing it will overwrite its contents."
msgstr ""

#: xfce4-panel-profiles/xfce4-panel-profiles.py:480
msgid "Replace"
msgstr "Алмастыру"

#: data/metainfo/org.xfce.PanelProfiles.appdata.xml.in:6
msgid "Xfce Panel Profiles"
msgstr "Xfce панель профильдері"

#: data/metainfo/org.xfce.PanelProfiles.appdata.xml.in:7
msgid "Application to manage different panel layouts in Xfce"
msgstr ""
"Xfce ішінде әр түрлі панель конфигурацияларын басқаруға арналған қолданба"

#: data/metainfo/org.xfce.PanelProfiles.appdata.xml.in:11
msgid ""
"Multitude of panel layouts can be created using Xfce panel. This tool "
"enables managing different layouts with little effort. Xfce4-panel-profiles "
"makes it possible to backup, restore, import and export panel layouts."
msgstr ""
"Xfce панелі көмегімен көптеген панель жаймаларын жасауға болады. Бұл құрал "
"әр түрлі жаймаларды оңай басқаруға мүмкіндік береді. Xfce4-panel-profiles "
"көмегімен панель жаймаларының қор көшірмесін жасау, қалпына келтіру, "
"импорттау және экспорттауға болады."
